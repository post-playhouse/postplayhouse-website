---
title: Industry Professionals
permalink: /industry-professionals/
layout: page
nav_title: Work at Post
group: main
#image: /images/2016/seasonslide2016.jpg
order: 7
---

![The outside front entrance of Post Playhouse]({{ site.baseurl }}/images/playhouse-pic-large.jpg)

## Summer 2019

Audition information regarding our 2019 season will be posted here as audition season approaches.
