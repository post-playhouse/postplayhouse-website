---
title: Ticket Information
permalink: /ticket-info/
layout: page
---

Ticket sales for every summer season begin November 1st of the previous calendar year. [Buy yours now](https://postplayhousetickets.universitytickets.com/user_pages/event_listings.asp)!

Or email us at [tickets@postplayhouse.com](mailto:tickets@postplayhouse.com)  
Call our box office at {{ site.box_office_phone }}


## Ticket Prices

<table id="ticket-prices" class="tickets" border="0" width="100%" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td colspan="1" scope="col"></td>
      <th colspan="2" scope="col">June</th>
      <th colspan="2" scope="col">July &amp; August</th>
    </tr>
    <tr>
      <td class="solid_under"></td>
      <td class="solid_under centered">Tues. - Thurs.</td>
      <td class="solid_under centered">Fri. - Sun.</td>
      <td class="solid_under centered">Tues. - Thurs.</td>
      <td class="solid_under centered">Fri. - Sun.</td>
    </tr>
    <tr class="dotted_under">
      <td>Adult</td>
      <td class="centered">$23</td>
      <td class="centered">$25</td>
      <td class="centered">$28</td>
      <td class="centered">$30</td>
    </tr>
    <tr class="dotted_under">
      <td>Senior <span class="info"> (65+)</span></td>
      <td class="centered">$21</td>
      <td class="centered">$23</td>
      <td class="centered">$26</td>
      <td class="centered">$28</td>
    </tr>
    <tr class="dotted_under">
      <td>Youth <span class="info"> (12-)</span></td>
      <td class="centered">$17</td>
      <td class="centered">$19</td>
      <td class="centered">$19</td>
      <td class="centered">$20</td>
    </tr>
    <tr>
      <td>Group <span class="info"> (25 or more)</span></td>
      <td class="centered">$19</td>
      <td class="centered">$21</td>
      <td class="centered">$25</td>
      <td class="centered">$27</td>
    </tr>
  </tbody>
</table>

### Group Rates

Groups of 25 or more may purchase tickets at the rate noted above. Tickets may be added to a group sale (if seats are available) up to show date but are never reduced or refunded. The Post Playhouse cannot be responsible for unused group tickets.

Group rate tickets can not be purchased online. Please email us for group rate ticket needs at <a href="mailto:tickets@postplayhouse.com">tickets@postplayhouse.com</a> or call our box office {{ site.box_office_phone }}.


## Subscription Pricing

Subscriptions are the best way to see what Post Playhouse has to offer each summer! With a Season Subscription, you get drastically discounted tickets on 4 or 5 different productions of your choosing. And since you don't have to pick your dates or seats at the time of purchase, they make great gifts.

<div class="subscription-pricing">
  <table id="subscription-prices" class="tickets" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <th colspan="3" scope="col">5 Show Subscriptions
          <div class="info">See 5 shows for less than the price of 4!</div>
        </th>
      </tr>
      <tr>
        <td width="33.333%" class="centered">Adult</td>
        <td width="33.333%" class="centered">Senior&nbsp;<span class="info">(65+)</span></td>
        <td width="33.333%" class="centered">Youth&nbsp;<span class="info">(12-)</span></td>
      </tr>
      <tr>
        <td class="centered">$110</td>
        <td class="centered">$105</td>
        <td class="centered">$80</td>
      </tr>
    </tbody>
  </table>

  <table id="subscription-prices" class="tickets" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <th colspan="3" scope="col">4 Show Season Subscriptions
          <div class="info">See 4 shows for nearly the price of 3!</div>
        </th>
      </tr>
      <tr>
        <td width="33.333%" class="centered">Adult</td>
        <td width="33.333%" class="centered">Senior <span class="info">(65+)</span></td>
        <td width="33.333%" class="centered">Youth <span class="info">(12-)</span></td>
      </tr>
      <tr>
        <td class="centered">$85</td>
        <td class="centered">$80</td>
        <td class="centered">$65</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="subscriber-alert">
  Subscribers: After purchasing your subscription, you can reserve your seats to any 4 or 5 performances by emailing us at <a href="mailto:tickets@postplayhouse.com">tickets@postplayhouse.com</a> or calling our box office {{ site.box_office_phone }}. Seats are based on availability, so please make your reservations as soon as you can.
</div>

### Season Subscriptions {{ site.season }}

Please choose from the following shows to fill out your season subscription:

{% assign productions = site.data.productions[site.season] | sort: "opening" %}
{%- for production in productions -%}
1. {{ production.title }}
{% endfor %}


Please remember that seats can only be reserved based on availability, so call the box office to secure your seats. Also note, as stated above, subscriptions may only be used on 4 or 5 *different productions*. This means they cannot be used to see the same production multiple times.

## Seating Chart

{% include seating-chart.html %}

## Ticket Policies

Possession of a ticket for a production at Post Playhouse entitles the holder a seat in the theatre at the performance indicated on the ticket, barring unforseen circumstances. The theatre reserves the right to move the patron to a seat other than the one indicated on their ticket if necessary at the theatre's discretion. The theatre reserves the right to refuse or revoke admittance to a patron holding a ticket at the theatre's discretion. Refunds may be made available in such circumstances, but are not guaranteed.

### Payment In Full Must Accompany Your Order

Online ticket orders may be paid with Visa or Master Card. Box office sales may be paid with credit card, check or cash. All orders will automatically be charged to your credit card. Reservations will not be held without payment in full.

### The Post Playhouse Cannot Cancel Or Refund Tickets

The Post Playhouse will make every effort to accommodate everyone with seats of their choice. However, if it is not possible, the best available seats will be substituted.

### Late Arrival

If a patron does not arrive at the theatre 10 minutes prior to curtain, the theatre reserves the right to resell that patron's seat to someone on the waitlist. In the event that a patron knows they are running late and cannot arrive before the 10 minute deadline, they can call the theatre and leave a message with the box office, letting the box office staff know that they are indeed on their way, and the box office will hold their seat. This call must be completed 15 minutes prior to curtain, or the staff may not be able to review the message before unclaimed seats are released.

If a patron arrives too late for seating before curtain, the theatre reserves the right to move the patron to a seat that will be more convenient for late seating. In such circumstances, the original seat may remain available for post-intermission seating, or may be given to another patron. In the latter situation, the patron will not be able to reclaim their original seat after intermission.

If a seat is resold at the box office due to a patron's absence, the patron who originally purchased the seat will be offered a gift certificate equal to the value of the resold seat.

If a patron misses a performance and their seat was not resold, no refund of any kind will be available.

If a patron arrives at the theatre too late to be seated before the show starts, the patron may be held in the lobby until an appropriate moment when the production and other patrons will be least disturbed by the late seating.

### Late Purchase and Waitlist

If a performance is sold out, an individual who wishes to be put on the waitlist may arrive at the theatre in person and ask to be added to the waitlist for that performance. Unclaimed seats will be released at 10 minutes prior to curtain. At that time, patrons on the waitlist may purchase the newly available seats. The individual on the waitlist must be present when their name is called, or they will be skipped and placed at the bottom of the list.
